Jxls
=====

Overview
--------
[Jxls](http://jxls.sf.net/) is a small and simple to use Java library for Excel generation.

Jxls abstracts Excel generation from underlying java-to-excel low-level processing library.

Jxls uses a special markup in Excel templates to define output formatting and data layout.

The module contains source code for [Jxls Core](http://jxls.sf.net/)

The bug
-------

Install, run example:

```
mvn install -DskipTests; mvn exec:java -pl jxls-examples -Dexec.mainClass="org.jxls.demo.MultiSheetBug"
```

This crashes.

The command generates `target/multi_sheet_bug.xls` from template at `jxls-examples/src/main/resources/org/jxls/demo/multisheet_bug_demo2.xls`.

Goals:
1. "Fix" the bug inside MultiSheetBug.java , test it out.
2. Once fixed, revert fix and patch the library so it:
  a) Does not crash
  b) Truncates the too-long sheet names
3. BONUS: Make sure both alice sheets are output

Useful to know:
- Excel has a hard-limit on sheet name length at 31 chars.
