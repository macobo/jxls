package org.jxls.demo;

import org.jxls.common.Context;
import org.jxls.demo.model.Department;
import org.jxls.demo.model.Employee;
import org.jxls.transform.poi.PoiTransformer;
import org.jxls.util.JxlsHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MultiSheetBug {
    static Logger logger = LoggerFactory.getLogger(MultiSheetBug.class);
    private static String template = "multisheet_bug_demo2.xls";
    private static String output = "target/multi_sheet_bug.xls";

    public static void main(String[] args) throws IOException {
        logger.info("Running Multiple Sheet Bug demo!");

        List<Employee> employees = Arrays.asList(
            new Employee("Alice", 20),
            new Employee("Bob", 30),
            new Employee("Alice", 27),
            new Employee("Claudia", 35),
            new Employee("PersonWithAVeryVeryLongNameNameeeeeeeeeeeeeeeeeeeeeeeeeee", 7)
        );

        List<String> sheetNames = Arrays.asList(
                "Alice",
                "Bob",
                "Alice",
                "Claudia",
                "PersonWithAVeryVeryLongNameNameeeeeeeeeeeeeeeeeeeeeeeeeee"
        );

        logger.info("Opening input stream");
        try (InputStream is = MultiSheetBug.class.getResourceAsStream(template)) {
            try (OutputStream os = new FileOutputStream(output)) {
                Context context = PoiTransformer.createInitialContext();
                context.putVar("employees", employees);
                context.putVar("sheetNames", sheetNames);
                JxlsHelper.getInstance().processTemplate(is, os, context);
            }
        }
    }

}
