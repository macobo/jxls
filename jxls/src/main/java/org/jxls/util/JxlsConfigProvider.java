package org.jxls.util;

public interface JxlsConfigProvider {
	
	String getProperty(String key, String defaultValue);
}
